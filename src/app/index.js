var container = document.getElementById('main-container');
var app = document.getElementById('mason-grid');
var Titles = document.getElementById('mason-title');
var tphotos = document.getElementById('tphotos');
var keyword = document.getElementById('keywords');
var keywords = '';
var loader = '';
var paging = 1;

if (keyword.value != '') {
    keywords = keyword.value;
} else {
    keywords = 'flowers';
} 
Titles.innerHTML = keywords;
    
function loadData(keywords, page) { 
    if (loader == '' || loader == null) {
        // set loader
        loader = document.createElement('div');
        loader.setAttribute('class', 'loader');
        container.appendChild(loader);

        setTimeout(function(){ 
            var request = new XMLHttpRequest();
            request.open('GET', 'https://api.unsplash.com/search/photos/?query='+ keywords +'&page='+page+'&quantity=10&client_id=ec92fea0de969a6a20c70e4610223752c538fe2b9d6d14ab8e4d36bcf177c600', true);
            request.onload = function () {

                // Begin accessing JSON data here
                var data = JSON.parse(this.response);
                if (request.status >= 200 && request.status < 400) {
                    let count_photos = 0;
                    if(data.total > 1000) {
                        count_photos = (data.total/1000).toFixed(1) + "k Photos";
                    } else {
                        count_photos = data.total + " Photos";
                    }
                    tphotos.innerHTML = count_photos;

                    data.results.forEach(masongrid => {
                        const items = document.createElement('div');
                        items.setAttribute('class', 'item');

                        const p = document.createElement('p');
                        p.textContent = masongrid.alt_description;

                        const imgs = document.createElement('img');
                        imgs.setAttribute('src', masongrid.urls.small);

                        let tags = document.createElement('ul');
                        tags.setAttribute('class', 'tags'); 
                        masongrid.photo_tags.forEach(taglist => {
                            const li = document.createElement('li');
                            const link = document.createElement('a');

                            link.textContent = taglist.title;
                            link.setAttribute('href', '#');
                            
                            li.appendChild(link);
                            tags.appendChild(li);
                        });

                        app.appendChild(items);
                        items.appendChild(imgs);
                        items.appendChild(p);
                        items.appendChild(tags);
                    });
                
                } else {
                    const errorMessage = document.createElement('marquee');
                    errorMessage.textContent = `it's not working!`;
                    app.appendChild(errorMessage);
                }

                loader = '';
                document.querySelector('.loader').remove();
            }
            request.send();

        }, 1500);    
    }
}

document.getElementById("btnSearch").onclick = function() {loadImage()};
function loadImage() {
    if (keyword.value != '') {
        keywords = keyword.value;
    } else {
        keywords = 'flowers';
    }
    setTimeout(function(){ 
        app.innerHTML = '';
    }, 500); 
    Titles.innerHTML = keywords;
    loadData(keywords, paging);
}


window.addEventListener("scroll", function(){
    // document.getElementById('showScroll').innerHTML = pageYOffset + 'px';

    let contentHeight = app.offsetHeight;
    let yOffset = window.pageYOffset;
    let y = yOffset + window.innerHeight;

    if (y >= contentHeight) {
        //load new content
        paging++;
        loadData(keywords, paging);
    }
});

window.onload = function () {
    loadData(keywords, paging);
};

