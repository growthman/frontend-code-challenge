# Front-End Interview Test

## The Task

The programming challenge exists in 2 parts:
* Part A: HTML + CSS
* Part B: Javascript

### PART A: HTML + CSS

* styling use preprocessor SCSS rendering with node-sass plugin based yarn tool.
* SCSS source put in folder src > app > scss
* rendering scss by running command yarn run node-sass src/app/scss/style.scss dist/css/main.css
* rendering source put in folder dist *

### PART B: Javascript

* Task B is writing using Pure Javascript ES6
* data images get from API https://api.unsplash.com/search/photos/?query=flowers&page=1&client_id=ec92fea0de969a6a20c70e4610223752c538fe2b9d6d14ab8e4d36bcf177c600.
* javascript source write on src/app/index.js.
* rendering javascript use webapack tool, by running command yarn run webpack
* javascript rendering file put on folder dist/js and rename to bundle.js *

## Additional Information

* Minus Feature Filter Categories
* Minus Feature Infinite Scrolling